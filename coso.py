import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.optimize import basinhopping


#General parameters
image_side = 5 #image will be image_sideximage_side pixels
N_images = 100000 #number of real images (to generate... here real images are still generated)

#Build a model to classify images into real/fake categories
model = Sequential()
model.add(Dense(20, input_shape=(image_side,image_side,1))) #image_sideximage_sidex1channel image
model.add(Flatten())
model.add(Activation('relu')) #try other activations
model.add(Dense(20))
model.add(Activation('relu')) #try other activations
model.add(Dense(2))
model.add(Activation('softmax'))
model.compile(loss = 'sparse_categorical_crossentropy', optimizer = 'adam', metrics=['accuracy'])

def classifier_performance(model, fake_images, real_images):
    '''Take a model in input, return its performance'''
    x_test = np.concatenate((fake_images, real_images), axis = 0)
    y_test = np.concatenate((np.zeros(fake_images.shape[0]), 1 + np.zeros(real_images.shape[0])))
    loss, acc = model.evaluate(x_test, y_test) 
    return abs(acc - 0.5) #0.5 is right only if fake_images.shape[0] = real_images.shape[0]

def generator(parameters, N_images_to_generate):
    '''Take parameters in input generate N images'''
    pixels = np.random.normal(loc = parameters[0], scale = max(0.0, parameters[1]),\
        size = N_images_to_generate*image_side*image_side)
    for i in range(0,len(pixels)):
    	pixels[i] = max(min(pixels[i], 1.0), 0.0)
    pixels.shape = [N_images_to_generate, image_side, image_side, 1]
    return(pixels)


real_images = generator([0.7, 0.1], N_images)
real_images.shape = [N_images, image_side, image_side, 1]

plt.imshow(real_images[np.random.randint(N_images),:,:,0], interpolation = 'nearest')
plt.set_cmap('Greys')
plt.axis('off')
plt.savefig('ass.jpeg', bbox_inches='tight')

def adv(model, parameters, real_images, batch_size, f_train):
	#Generate fake images and select some real images at random
    fake_images = generator(parameters, batch_size) #generate batch_size fake images

    plt.imshow(fake_images[np.random.randint(batch_size),:,:,0], interpolation = 'nearest')
    plt.set_cmap('Greys')
    plt.axis('off')
    plt.savefig('face.jpeg', bbox_inches='tight')

    np.random.shuffle(real_images) #shuffle real images at random
    selected_real_images = real_images[0:batch_size,:,:,:] #select the first batch_size real images
    #Split into train and test
    f_batch_size = int(round(f_train*batch_size)) #number of train images / total numer = f
    x_train = np.concatenate((fake_images[0:f_batch_size,:,:,:], selected_real_images[0:f_batch_size,:,:,:]), axis = 0)
    y_train = np.concatenate((np.zeros(f_batch_size), 1 + np.zeros(f_batch_size)))
    model.fit(x_train, y_train, shuffle = True, epochs = 3)
    perf = classifier_performance(model, fake_images[f_batch_size:,:,:,:],\
        selected_real_images[f_batch_size:,:,:,:])
    return perf

def adv_mean(model, parameters, real_images, batch_size, f_train):
    jee = []
    for i in range(0,100):
        jee.append(adv(model, parameters, real_images, batch_size, f_train))
	return np.mean(np.array(jee))

def optimizer(model, batch_size, real_images):
    initial_parameters = np.array([0.6, 0.1])
    def f(parameters):
        a = adv_mean(model, parameters, real_images, batch_size, 0.5)
        return a
    #return minimize(f, initial_parameters, options={'maxiter':10}, bounds = [[0.0,1.0],[0.0, 0.2]])
    return basinhopping(f, initial_parameters, niter = 20)

A = []
I = []
J = []
iis = np.arange(0, 1, 0.1)
jis = np.arange(0.05, 0.2, 0.05)
for i in iis:
    for j in jis:
        A.append(adv_mean(model, [i, j], real_images, 2000, 0.5))
        I.append(i)
        J.append(j)

A = np.array(A)
A.shape = [len(iis), len(jis)]
J = np.array(J)
J.shape = [len(iis), len(jis)]
I = np.array(I)
I.shape = [len(iis), len(jis)]

print A
print I
print J

print optimizer(model, 2000, real_images)

