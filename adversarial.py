#!/usr/local/bin/python
import numpy as np
from scipy.stats import ks_2samp as ks
from scipy.optimize import minimize
N = 25 #numero di pixel dell'immagine

def generator(parameters):
    '''pick a simulation from MOCCA database, drive COCOA to make a mock'''
    sigma = min([abs(parameters[1]), 2])
    mu = max([min([parameters[0], 1]),-1])
    img = np.random.normal(loc=mu, scale=sigma, size = N)
    return(img)

def trainer(real_img_train, mock_img_train):
    '''receive in input real and mock images, train a model, output the trained model'''
    return("no model for now")

def performance(model, real_img_test, mock_img_test):
    '''will call classifier to calculate the predicted probability 
    (by the model) on real and fake images and compute a performance metric'''
    x = np.array(real_img_test).flatten()
    y = np.array(mock_img_test).flatten()
    performance = -(1-(ks(x,y).pvalue)) #big p-value = bad discrimination
    return(performance)

def initialize_parameters():
    '''Randomly generates initial parameters'''
    parameters = np.random.random(2)
    parameters[0] -= 0.5
    parameters[1] *= 2.0 
    return(parameters)

def perturb(parameters):
    '''Generate small perturbations to the parameters'''
    perturbed_parameters = parameters #I actually don't touch them; samples will differ because seed
    return(perturbed_parameters)

def sample(img, N):
    '''randomly returns N images from a list of images'''
    if len(img) < N: raiseValueError("Batch size should be smaller than the total number of real images")
    selected_img = []
    for i in np.random.choice(range(0, len(img)), N, replace = 0):
        selected_img.append(img[i])
    return selected_img

def adversarial(parameters, batch_size, real_img):
    '''Runs the adversarial generation cycle'''
    mock_img = [generator(parameters)]
    for i in range(0, 2*batch_size-1):
        parameters = perturb(parameters)
        mock_img.append(generator(parameters))
    real_img_trainandtest = sample(real_img, 2*batch_size)
    real_img_train = real_img_trainandtest[:batch_size]
    real_img_test = real_img_trainandtest[batch_size:]
    mock_img_train = mock_img[:batch_size]
    mock_img_test = mock_img[batch_size:]    
    model = trainer(real_img_train, mock_img_train)
    return performance(model, real_img_test, mock_img_test)

real_parameters = np.array([0.4, 1.8])
real_img = [generator(real_parameters)]
for i in range(0, 10000):
    real_img.append(generator(real_parameters))

p = adversarial(initialize_parameters(), 100, real_img)
print p

p = adversarial(np.array([0.39, 1.81]), 100, real_img)
print p
 
def optimize(f, initial_parameters, target):
    '''Some optimizer: simulated annealing for example'''
    return minimize(f, initial_parameters, method = 'Nelder-Mead', options={'maxiter':10000})

def optimizer(target_performance, batch_size, real_img):
    initial_parameters = np.array([0.45, 1.7]) #initialize_parameters()
    def f(parameters):
        return adversarial(parameters, batch_size, real_img)
    return optimize(f, initial_parameters, target_performance)

print optimizer(0.0, 10, real_img)
